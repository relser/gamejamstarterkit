using System;
using UnityEngine.Events;

namespace GameJamStarterKit
{
    /// <summary>
    /// UnityEvent with a float parameter
    /// </summary>
    [Serializable]
    public class UnityFloatEvent : UnityEvent<float> { }
}