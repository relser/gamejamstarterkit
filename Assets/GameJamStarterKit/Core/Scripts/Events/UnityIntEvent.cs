using System;
using UnityEngine.Events;

namespace GameJamStarterKit
{
    /// <summary>
    /// UnityEvent with a int parameter
    /// </summary>
    [Serializable]
    public class UnityIntEvent : UnityEvent<int> { }
}