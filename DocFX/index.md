# Game Jam Starter Kit
Game Jam Starter Kit is a collection of useful extensions, systems, and assets designed to help teams avoid writing boilerplate, or wasting time making throw away placeholder assets.

# What Game Jam Starter Kit isn't
This is not a collection of pre-made games, gameplay systems, or other 'game in a box' style tools. Starter Kit is intended to help you make a game from scratch. 

# How can I get it?
Ensure you have git [https://git-scm.com/](https://git-scm.com/)

If you're starting a brand new project, and want to use all of the modules, checkout the entire repo and open with whatever version of unity. (This is being built with 2019.2 at the moment)

`git checkout https://gitlab.com/ASeward/gamejamstarterkit.git`

If you have an existing project, or don't know what modules you want to use yet, check the branches for this repo to pick and choose what modules you want.

Each module branch has a readme explaining how to add it to your project. 

**UPM Does not support git dependencies per-package (yet). Ensure you add the core branch before adding other modules or you'll get errors. Each module has instructions for adding the core module.**

coming soon(tm) `GameJamStarterKit.Updater` will be able to handle installation and updating the project at least until UPM supports updating packages.

### My UPM doesn't support git urls! 
for `<=2018.2` package manager does not support git urls. Your best bet is either upgrading to `>=2018.3` or cloning the entire repo and copying the modules you want to use.
 
 
# how do I update git UPM packages!?
UPM locks to whatever the latest commit was when you added the project. 

##  \>=2019.3
if you already have `Packages\packages-lock.json`, delete the file and unity should update to the latest version for all your git packages.

if you do not have `packages-lock.json` continue

Add this to your `Packages\manifest.json` after `"dependencies": { ... }`

```
  "enableLockFile": true,
  "resolutionStrategy": "highest",
```
If there is a `"lock:" { ... }` section, delete it.

so your `Packages\manifest.json` looks like this 
```json5
{
  "dependencies": {
    "com.aseward.game-jam-starter-kit.core": "https://gitlab.com/ASeward/gamejamstarterkit.git#core",
  },
  "enableLockFile": true,
  "resolutionStrategy": "highest",
}
```

**What did I just do?**

This forces UPM to use a packages-lock.json file in the future.

essentially a file for that `"lock:" { ... }` section.

If you removed the `"lock": { ... }` section, or didn't have it, you should be safe to open unity and it will update to the latest version of the git package.

**In the future, delete `Packages\packages-lock.json` and it should update your git packages.**

## \<=2019.2

In `Packages\manifest.json` locate the section for `"lock": { ... }`.

It will look something like this 

```json5
  "lock": {
    "com.aseward.game-jam-starter-kit.core": {
      "hash": "b93a03f45e25e075987d50157ffe0c0960607aa6",
      "revision": "HEAD"
    }
```

remove the entire section, save, and start unity.

# Links
[GitLab - Issue Tracker](https://gitlab.com/ASeward/gamejamstarterkit/issues)

[Documentation / Manual](https://aseward.gitlab.io/gamejamstarterkit/)

[API Reference](https://aseward.gitlab.io/gamejamstarterkit/api/GameJamStarterKit.html)

[Discord](https://discord.gg/zXs5MCb)