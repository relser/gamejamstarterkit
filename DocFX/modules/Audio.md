# Core 
`GameJamStarterKit.Audio` is a package with useful audiio clips, components, pools, and other utilities to help with implementing audio features in a small scope game.

# Features
* AudioPool - Subclass of `Core.BasePool`providing an easy way to play 2d and 3d audio clips anywhere.
* Persistent/BackgroundMusic provides a simple class to play single background music clips / ClipCollections, either persisting through scene change or not.

